package com.example.footballteam.util;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.footballteam.R;
import com.example.footballteam.data.Team;
import java.util.List;

public class SavedListAdapter  extends RecyclerView.Adapter<SavedListAdapter.ViewHolder> {
    private List<Team> teams;
    private clickListener clickListener;

    public interface clickListener {
        void qrButtonClick(int num);
        void onElementClick(int num);
        void deleteTeam(String name);
    }

    public SavedListAdapter(List<Team> data) {
        this.teams = data;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView name;
        private ImageButton qrButton;
        private ImageButton deleteButton;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.recycler_item_name);
            qrButton = itemView.findViewById(R.id.recycler_item_qr_button);
            deleteButton = itemView.findViewById(R.id.delete_button);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            clickListener.onElementClick(getAdapterPosition());
        }
    }

    @NonNull
    @Override
    public SavedListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item, parent, false);
        return new SavedListAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SavedListAdapter.ViewHolder holder, int position) {
        holder.name.setText(teams.get(position).getName());
        holder.qrButton.setOnClickListener(v-> clickListener.qrButtonClick(position));
        holder.deleteButton.setOnClickListener(v -> {
            clickListener.deleteTeam(teams.get(position).getName());
            teams.remove(position);
            notifyDataSetChanged();
        });
    }

    @Override
    public int getItemCount() {
        if(teams.isEmpty()) return 0;
        else return teams.size();
    }

    public void setListener(SavedListAdapter.clickListener clickListener) {
        this.clickListener = clickListener;
    }

}
