package com.example.footballteam.util;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.footballteam.R;
import com.example.footballteam.data.GalleryImage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.List;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {
    private List<GalleryImage> gallery;

    public GalleryAdapter(List<GalleryImage> data) {
        this.gallery = data;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private ImageView qrImage;
        private ImageButton deleteButton;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.gallery_item_name);
            qrImage = itemView.findViewById(R.id.gallery_qr);
            deleteButton = itemView.findViewById(R.id.gallery_deleteButton);
        }
    }

    @NonNull
    @Override
    public GalleryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.gallery_item, parent, false);
        return new GalleryAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull GalleryAdapter.ViewHolder holder, int position) {
        holder.qrImage.setImageBitmap(gallery.get(position).getBitmap());
        try {
            String team = DataUtil.readQr(gallery.get(position).getBitmap());
            JSONObject object = new JSONObject(team);
            holder.name.setText(object.getString("name"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        holder.deleteButton.setOnClickListener(v -> {
            File file = new File(gallery.get(position).getPath());
            file.delete();
            gallery.remove(position);
            notifyDataSetChanged();
        });
    }

    @Override
    public int getItemCount() {
        if(gallery.isEmpty()) return 0;
        else return gallery.size();
    }

}
