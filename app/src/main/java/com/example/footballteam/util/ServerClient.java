package com.example.footballteam.util;


import com.example.footballteam.data.SplashResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ServerClient {

    @FormUrlEncoded
    @POST("FootballTeam/{file}")
    Call<SplashResponse> getSplash(@Path("file") String file, @Field("locale") String locale);

}
