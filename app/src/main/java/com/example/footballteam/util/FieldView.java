package com.example.footballteam.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.VectorDrawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.example.footballteam.R;
import com.example.footballteam.data.Player;
import com.example.footballteam.data.Team;

import java.security.SecureRandom;
import java.util.Random;

public class FieldView extends View {
    private Context context;
    private Bitmap playerBitmap = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.png),
            80,90,false);
    private Bitmap goalkeeperBitmap;
    private Bitmap ballBitmap;
    private float screenHeight, screenWidth;
    private Team team;
    private float moveX = 0, moveY = 0;
    private int movePlayer  = -1;
    private float ballX = 0 , ballY = 0;

    public FieldView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        goalkeeperBitmap = paintShirt(Color.YELLOW);
        ballBitmap = DataUtil.svgToBitmap((VectorDrawable) ContextCompat.getDrawable(context, R.drawable.ic_soccer_ball));
        }

    @Override
    protected void onDraw(Canvas c) {
        drawField(c);
        drawPlayers(c);
    }

    @Override
    protected void onSizeChanged(int xNew, int yNew, int xOld, int yOld){
        super.onSizeChanged(xNew, yNew, xOld, yOld);
        if (screenHeight == 0) {
            screenHeight = this.getHeight();
            screenWidth = this.getWidth();
            ballX = screenWidth / 2 - ((float)ballBitmap.getWidth() / 2);
            ballY = screenHeight / 2 - ((float)ballBitmap.getHeight() / 2);
        } else {
            screenWidth = xNew;
            screenHeight = yNew;
        }
        invalidate();
    }

    @Override
    public boolean onTouchEvent (MotionEvent event){

        if(event.getActionMasked() == MotionEvent.ACTION_DOWN){
            moveX = event.getX();
            moveY = event.getY();
            movePlayer = locateObject((int)event.getX(), (int)event.getY());
            return true;
        }else if(event.getActionMasked() == MotionEvent.ACTION_MOVE){
            if(movePlayer == 12) {              //moving ball
                ballX += event.getX() - moveX;
                ballY += event.getY() - moveY;
                moveX = event.getX();
                moveY = event.getY();
                this.invalidate();
            }
            else if(movePlayer != -1) {         //moving player
                team.getPlayer(movePlayer).moveToDelta(getDeltaX(movePlayer, (float)team.getPlayer(movePlayer).x + event.getX() -moveX),
                        getDeltaY(movePlayer, (float)team.getPlayer(movePlayer).y + event.getY() - moveY));
                moveX = event.getX();
                moveY = event.getY();
                this.invalidate();
            }
            return true;
        } else if(event.getActionMasked() == MotionEvent.ACTION_UP) {
            movePlayer = -1;
        }
        return super.onTouchEvent(event);
    }

    public void changeColor(int color) {
        playerBitmap = paintShirt(color);
        team.setColor(color);
        invalidate();
    }

    public void changeName(int player, String name) {
        team.getPlayer(player).name = name;
        invalidate();
    }

    private void teamRandom() {
        team = new Team();
        for(int i = 0; i < 10; i++) {
            Random r = new SecureRandom();
            float randomX = 10 + r.nextFloat() * (screenWidth - 10 - playerBitmap.getWidth());
            float randomY = (screenHeight / 2) + r.nextFloat() * (screenHeight / 2 - 10 - playerBitmap.getHeight());
            team.setPlayer(i, new Player(randomX, randomY));
        }
        team.setPlayer(10, new Player(screenWidth / 2 - (float)goalkeeperBitmap.getWidth() / 2, screenHeight - 10 - goalkeeperBitmap.getHeight()));
        team.setColor(R.color.red);
        //invalidate();
    }

    public void setTeam(Team team) {
        this.team = team;
        playerBitmap = paintShirt(team.getColor());
        invalidate();
    }

    public Team getTeam() {
        return team;
    }

    private Bitmap paintShirt(int color) {
        Bitmap temp = Bitmap.createBitmap(playerBitmap.getWidth(),playerBitmap.getHeight(), Bitmap.Config.ARGB_8888);
        float[] pHSV = new float[3];
        float[] dstHSV = new float[3];
        Color.colorToHSV(color, dstHSV);
        if(playerBitmap != null)
            for(int i = 0; i < playerBitmap.getWidth(); i++) {
                for(int j = 0; j < playerBitmap.getHeight(); j++) {
                    int pixelColor = playerBitmap.getPixel(i, j);
                    int pixelAlpha = Color.alpha(pixelColor);
                    Color.colorToHSV(pixelColor,pHSV);
                    dstHSV[1] = pHSV[1];
                    dstHSV[2] = pHSV[2];
                    if(pixelColor != Color.WHITE)
                        temp.setPixel(i, j, Color.HSVToColor(pixelAlpha, dstHSV));
                    else temp.setPixel(i, j, pixelColor);
                }
            }
        return temp;
    }

    private void drawPlayers(Canvas c) {
        if(team == null) teamRandom();
        Paint p = new Paint();
        p.setColor(Color.WHITE);
        p.setTextAlign(Paint.Align.CENTER);
        p.setTextSize(35);
        p.setFakeBoldText(true);
        for(int i = 0; i < 10; i++) {
            c.drawBitmap(playerBitmap, (float)team.getPlayer(i).x, (float)team.getPlayer(i).y, null);
            c.drawText(team.getPlayer(i).name,(float)team.getPlayer(i).x + ((float)playerBitmap.getWidth() / 2), (float)team.getPlayer(i).y + playerBitmap.getHeight() + 20, p);
        }

        c.drawBitmap(goalkeeperBitmap, (float)team.getPlayer(10).x, (float)team.getPlayer(10).y, null);
        c.drawText(team.getPlayer(10).name,(float)team.getPlayer(10).x + ((float)playerBitmap.getWidth() / 2), (float)team.getPlayer(10).y + playerBitmap.getHeight() + 20, p);

    }

    private void drawField(Canvas c) {
        Paint p = new Paint();
        p.setStrokeWidth(2);
        p.setColor(Color.WHITE);
        p.setStyle(Paint.Style.STROKE);
        c.drawRect(10,10,screenWidth - 10,screenHeight - 10, p);
        c.drawLine(10, screenHeight / 2, screenWidth - 10, screenHeight / 2, p);
        c.drawCircle(screenWidth / 2, screenHeight / 2, 100, p);

        float goalWidth = (screenWidth - 20) / 2;
        float goalHeight = (screenHeight - 20) / 6;
        c.drawRect((goalWidth / 2) + 10, screenHeight - 10 - goalHeight, screenWidth - 10 - (goalWidth / 2), screenHeight - 10, p);

        float goalWidth2 = goalWidth / 4;
        float goalHeight2 = goalHeight / 2;
        c.drawRect((screenWidth / 2) - goalWidth2, screenHeight - 10 - goalHeight2, (screenWidth / 2) + goalWidth2, screenHeight - 10, p);

        float radius = goalWidth / 3;
        RectF oval = new RectF((screenWidth / 2) - radius,screenHeight - 10 - goalHeight - radius,
                (screenWidth / 2) + radius,screenHeight - 10 - goalHeight + radius);
        c.drawArc(oval,180F,180F, true, p);

        c.drawRect((goalWidth / 2) + 10, 10, screenWidth - 10 - (goalWidth / 2), 10 + goalHeight, p);
        c.drawRect((screenWidth / 2) - goalWidth2, 10, (screenWidth / 2) + goalWidth2,  10 + goalHeight2, p);
        oval = new RectF((screenWidth / 2) - radius,10 + goalHeight - radius,
                (screenWidth / 2) + radius,10 + goalHeight + radius);
        c.drawArc(oval,0F,180F, true, p);

        // Draw ball
        c.drawBitmap(ballBitmap, ballX, ballY, null);
    }

    private int locateObject(int x, int y) {
        if(x > ballX && x < ballX + ballBitmap.getWidth() && y > ballY && y < ballY + ballBitmap.getHeight()) return 12;
        else return findPlayer(x, y);
    }

    public int findPlayer(int x, int y) {
        for(int i = 0; i < 11; i++) {
            if(x > team.getPlayer(i).x && x < (team.getPlayer(i).x + playerBitmap.getWidth()) &&
            y > team.getPlayer(i).y && y < (team.getPlayer(i).y + playerBitmap.getHeight())) return i;
        }
        return -1;
    }

    private float  getDeltaX(int player, float x) {
        float deltaX;
        if(player == 10) {
            float s = (screenWidth - 20) / 4;
            if(x < 10 + s) deltaX = 10 + s - (float)team.getPlayer(player).x;
            else if(x > screenWidth - 10 - playerBitmap.getWidth() - s) deltaX = screenWidth - 10 - playerBitmap.getWidth() - s - (float)team.getPlayer(player).x;
            else deltaX = x - (float)team.getPlayer(player).x;

        }
        else {
            if(x < 10) deltaX = 10 - (float)team.getPlayer(player).x;
            else if(x > screenWidth - 10 - playerBitmap.getWidth()) deltaX = (screenWidth - 10 - playerBitmap.getWidth()) - (float)team.getPlayer(player).x;
            else deltaX = x - (float)team.getPlayer(player).x;

        }
        return deltaX;
    }

    private float  getDeltaY(int player, float y) {
        float deltaY;
        if(player == 10) {
            float s = (screenHeight - 20) / 6;
            if(y < screenHeight - 10 - s) deltaY = screenHeight - 10 - s - (float)team.getPlayer(player).y;
            else if(y > screenHeight - 10 - playerBitmap.getHeight()) deltaY = screenHeight - 10 - playerBitmap.getHeight() - (float)team.getPlayer(player).y;
            else deltaY = y - (float)team.getPlayer(player).y;

        }
        else {
            if(y < screenHeight / 2 ) deltaY = screenHeight / 2  - (float)team.getPlayer(player).y;
            else if(y > screenHeight - 10 - playerBitmap.getHeight()) deltaY = screenHeight - 10 - playerBitmap.getHeight() - (float)team.getPlayer(player).y;
            else deltaY = y - (float)team.getPlayer(player).y;

        }
        return deltaY;
    }

}
