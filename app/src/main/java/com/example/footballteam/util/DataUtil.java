package com.example.footballteam.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.VectorDrawable;


import com.example.footballteam.data.GalleryImage;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.RGBLuminanceSource;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeWriter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class DataUtil {

    private static void saveFile(Context context, String string) {
        File file = new File(context.getFilesDir(), "my_data.txt");

        FileOutputStream outputStream;
        try {
            file.createNewFile();

            outputStream = new FileOutputStream(file, false);

            outputStream.write(string.getBytes());
            outputStream.flush();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getData(Context context) {
        File file = new File(context.getFilesDir(),"my_data.txt");

        StringBuilder text = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
            }
            br.close();
            return text.toString();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void saveTeam(Context context, JSONObject team) {
        String existingData = getData(context);
        JSONArray array;
        try{
            array = new JSONArray(existingData);
            array.put(team);
        } catch (JSONException e) {
            array = new JSONArray();
            array.put(team);
        }
        saveFile(context, array.toString());
    }

    public static void deleteTeam(Context context, String teamName) {
        String existingData = getData(context);
        JSONArray array;
        try{
            array = new JSONArray(existingData);
            for(int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                if(object.getString("name").equals(teamName)) array.remove(i);
            }
        } catch (JSONException e) {
            array = new JSONArray();
        }
        saveFile(context, array.toString());
    }

    public static Bitmap stringToBitmap(String string) {
        QRCodeWriter writer = new QRCodeWriter();

        try {
            Map<EncodeHintType,Object> tmpHintsMap = new EnumMap<>(EncodeHintType.class);
            tmpHintsMap.put(EncodeHintType.CHARACTER_SET,"utf-8");

            BitMatrix bitMatrix = writer.encode(string, BarcodeFormat.QR_CODE, 512, 512, tmpHintsMap);
            int width = bitMatrix.getWidth();
            int height = bitMatrix.getHeight();
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                }
            }
            return bmp;
        } catch (WriterException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void saveToGallery(Context context, Bitmap bitmap) {
        File gallery = new File(context.getFilesDir() + "/gallery");
        if (!gallery.exists())
            gallery.mkdir();
        String name = UUID.randomUUID().toString() + ".png";
        File file = new File(gallery, name);
        FileOutputStream outputStream = null;
        try {
            file.createNewFile();
            outputStream = new FileOutputStream(file, false);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static List<GalleryImage> loadGallery(Context context) {
        List<GalleryImage> gallery = new ArrayList<>();
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        String path = context.getFilesDir() + "/gallery/";
        File directory = new File(path);
        File[] files = directory.listFiles();
        if(files != null)
            for (File file: files) {
                Bitmap image = BitmapFactory.decodeFile(file.getAbsolutePath(), options);
                if(image != null)
                    gallery.add(new GalleryImage(image, file.getAbsolutePath()));
            }
        return gallery;
    }

    public static String readQr(Bitmap qr) {
        String string = null;

        int[] intArray = new int[qr.getWidth() * qr.getHeight()];

        qr.getPixels(intArray, 0, qr.getWidth(), 0, 0, qr.getWidth(), qr.getHeight());

        RGBLuminanceSource source = new RGBLuminanceSource(qr.getWidth(), qr.getHeight(), intArray);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

        MultiFormatReader reader = new MultiFormatReader();
        try {

            Map<DecodeHintType,Object> tmpHintsMap = new EnumMap<>(DecodeHintType.class);
            tmpHintsMap.put(DecodeHintType.CHARACTER_SET,"utf-8");
            tmpHintsMap.put(DecodeHintType.PURE_BARCODE, Boolean.FALSE);

            string = reader.decode(bitmap, tmpHintsMap).getText();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return string;
    }

    public static Bitmap svgToBitmap(VectorDrawable vectorDrawable) {
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),
                vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        vectorDrawable.draw(canvas);
        return bitmap;
    }
}
