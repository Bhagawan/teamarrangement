package com.example.footballteam;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.footballteam.data.Team;
import com.example.footballteam.util.DataUtil;
import com.example.footballteam.util.FieldView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    private FieldView field;
    private Target target;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fullscreen();
        loadBackground();

        field = findViewById(R.id.field);

        Bundle b = getIntent().getExtras();
        String team;
        if(!(b == null)) {
            team = b.getString("team");
            try{
                JSONObject object = new JSONObject(team);
                field.setTeam(new Team(object));
            } catch(JSONException e) {
                Toast.makeText(this, R.string.error, Toast.LENGTH_SHORT).show();
            }
        }

        Toolbar toolbar = findViewById(R.id.field_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator( R.drawable.ic_baseline_arrow_back_24);
        getSupportActionBar().setDisplayShowTitleEnabled( false );
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if ( v instanceof EditText) {
                Rect outRect = new Rect();
                v.getGlobalVisibleRect(outRect);
                if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                    v.clearFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
        }
        fullscreen();
        return super.dispatchTouchEvent( event );
    }

    @Override
    public boolean onOptionsItemSelected( MenuItem item ) {
        if(item.getItemId() == android.R.id.home) {
            exit();
            return true;
        } else if(item.getItemId() == R.id.field_changeColor) {
            chooseColor();
            return true;
        } else if(item.getItemId() == R.id.field_changeName) {
            changeName();
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.field_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void chooseColor() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        View currAlert = getLayoutInflater().inflate(R.layout.colors_choice, null);
        LinearLayout red = currAlert.findViewById(R.id.color_red);
        red.setOnClickListener(v-> field.changeColor(getResources().getColor(R.color.red)));

        LinearLayout purple = currAlert.findViewById(R.id.color_purple);
        purple.setOnClickListener(v-> field.changeColor(getResources().getColor(R.color.purple)));

        LinearLayout dBlue = currAlert.findViewById(R.id.color_dark_blue);
        dBlue.setOnClickListener(v-> field.changeColor(getResources().getColor(R.color.dark_blue)));

        LinearLayout lBlue = currAlert.findViewById(R.id.color_light_blue);
        lBlue.setOnClickListener(v-> field.changeColor(getResources().getColor(R.color.light_blue)));

        LinearLayout green = currAlert.findViewById(R.id.color_green);
        green.setOnClickListener(v-> field.changeColor(getResources().getColor(R.color.green)));

        LinearLayout yellow = currAlert.findViewById(R.id.color_yellow);
        yellow.setOnClickListener(v-> field.changeColor(getResources().getColor(R.color.yellow)));

        LinearLayout orange = currAlert.findViewById(R.id.color_orange);
        orange.setOnClickListener(v-> field.changeColor(getResources().getColor(R.color.orange)));

        LinearLayout teal = currAlert.findViewById(R.id.color_teal);
        teal.setOnClickListener(v-> field.changeColor(getResources().getColor(R.color.teal_700)));

        builder.setView(currAlert);
        builder.setPositiveButton(getResources().getString(R.string.Ok), (dialog, which) -> dialog.dismiss());

        AlertDialog alertDialog = builder.create();

        alertDialog.setOnShowListener(dialog -> fullscreen());
        alertDialog.show();

        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.light_gray)));

    }

    @SuppressLint("ClickableViewAccessibility")
    private void changeName() {
        Toast.makeText(this, R.string.choose_player, Toast.LENGTH_SHORT).show();
        field.setOnTouchListener((v, event) -> {
            int p = field.findPlayer((int)event.getX(),(int)event.getY());
            if(p >= 0) {
                showNameChangeDialog(p);
                field.setOnTouchListener(null);
            }
            return true;
        });
    }
    private void showNameChangeDialog(int player) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        View currAlert = getLayoutInflater().inflate(R.layout.name_change, null);
        EditText input = currAlert.findViewById(R.id.change_name_input);

        builder.setView(currAlert);

        builder.setNeutralButton(getResources().getString(R.string.cancel), (dialog, which)-> dialog.dismiss());
        builder.setPositiveButton(getResources().getString(R.string.Ok), (dialog, which) -> {
            field.changeName(player, input.getText().toString());
            dialog.dismiss();
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(dialog -> fullscreen());
        alertDialog.show();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.light_gray)));

    }

    private void fullscreen() {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }


    private void exit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.save_question);
        builder.setNegativeButton(getResources().getString(R.string.no), (dialog, which) -> finish());
        builder.setPositiveButton(getResources().getString(R.string.yes), (dialog, which) -> {
            nameInput();
            dialog.dismiss();
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(dialog -> fullscreen());
        alertDialog.show();
    }

    private void nameInput() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        View currAlert = getLayoutInflater().inflate(R.layout.save_alert, null);
        EditText input = currAlert.findViewById(R.id.name_input);

        builder.setView(currAlert);
        builder.setNegativeButton(getResources().getString(R.string.cancel), (dialog, which) -> {
        });
        builder.setPositiveButton(getResources().getString(R.string.yes), (dialog, which) -> {
            Team team = field.getTeam();
            team.setName(input.getText().toString());
            try {
                DataUtil.saveTeam(this, team.toJson());
            } catch (JSONException e) {
                Toast.makeText(this, R.string.error,Toast.LENGTH_SHORT).show();
            }
            finish();
            dialog.dismiss();
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(dialog -> fullscreen());
        alertDialog.show();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.light_gray)));
    }

    private void loadBackground() {
        FieldView fieldView = findViewById(R.id.field);
        target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                fieldView.setBackground(new BitmapDrawable(getResources(),bitmap));
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };

        Picasso.get().load("http://159.69.89.54/FootballTeam/field.png").into(target);
    }

}