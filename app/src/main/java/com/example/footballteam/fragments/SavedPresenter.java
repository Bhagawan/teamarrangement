package com.example.footballteam.fragments;

import android.content.Context;

import com.example.footballteam.data.Team;
import com.example.footballteam.util.DataUtil;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import moxy.InjectViewState;
import moxy.MvpPresenter;


@InjectViewState
public class SavedPresenter extends MvpPresenter<SavedPresenterViewInterface> {

    public void setRecycler(Context context) {
        try {
            JSONArray array = new JSONArray(DataUtil.getData(context));
            ArrayList<Team> teams = new ArrayList<>();
            for(int i = 0; i < array.length(); i++) {
                teams.add(new Team(array.getJSONObject(i)));
            }
            getViewState().showSaved(teams);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
