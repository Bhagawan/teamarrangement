package com.example.footballteam.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.activity.result.ActivityResultLauncher;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.footballteam.MainActivity;
import com.example.footballteam.R;
import com.journeyapps.barcodescanner.ScanContract;
import com.journeyapps.barcodescanner.ScanOptions;


public class MenuFragment extends Fragment {
    private View view;

    private final ActivityResultLauncher<ScanOptions> barcodeLauncher = registerForActivityResult(new ScanContract(),
            result -> {
                if(!(result.getContents() == null)) {
                    openField(result.getContents());
                }
            });

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_menu, container, false);

        Button createNew = view.findViewById(R.id.main_menu_new_button);
        createNew.setOnClickListener(v-> openField(""));

        Button saved = view.findViewById(R.id.main_menu_tactics_button);
        saved.setOnClickListener(v->{
            FragmentTransaction ft = getParentFragmentManager().beginTransaction();
            ft.replace(R.id.fragmentContainerView, new SavedFragment()).addToBackStack(null).commit();
        });

        Button scan = view.findViewById(R.id.main_menu_import_button);
        scan.setOnClickListener(v -> scanQr());
        return view;
    }

    private void openField(String team) {
        Intent i = new Intent(getContext(), MainActivity.class);
        if(!team.isEmpty()) {
            Bundle b = new Bundle();
            b.putString("team", team);
            i.putExtras(b);
        }
        i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(i);
    }

    private void scanQr() {
        ScanOptions options = new ScanOptions();
        options.setDesiredBarcodeFormats(ScanOptions.QR_CODE);
        options.setPrompt("");
        options.setOrientationLocked(false);
        options.setBeepEnabled(false);
        options.setBarcodeImageEnabled(false);
        barcodeLauncher.launch(options);
    }

}