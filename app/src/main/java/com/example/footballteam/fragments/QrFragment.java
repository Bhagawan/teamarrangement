package com.example.footballteam.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.example.footballteam.R;
import com.example.footballteam.util.DataUtil;



public class QrFragment extends Fragment {
    private  View view;
    private String object = "";

    public QrFragment(String object) {
        this.object = object;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_qr, container, false);

        Bitmap qr = DataUtil.stringToBitmap(object);
        if(qr != null) {
            ImageView imageView = view.findViewById(R.id.qr_image);
            imageView.setImageBitmap(qr);
        }
        ConstraintLayout c = view.findViewById(R.id.qr_fragment);
        c.setOnClickListener(v -> getParentFragmentManager().popBackStack());

        ImageButton button = view.findViewById(R.id.save_button);
        button.setOnClickListener(v -> saveQr(qr));
        return view;
    }

    private void saveQr(Bitmap image) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(R.string.save_question);
        builder.setNegativeButton(getResources().getString(R.string.no), (dialog, which) -> {

        });
        builder.setPositiveButton(getResources().getString(R.string.yes), (dialog, which) -> {
            DataUtil.saveToGallery(getContext(), image);
            dialog.dismiss();
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(dialog -> fullscreen());
        alertDialog.show();
    }

    private void fullscreen() {
        getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_IMMERSIVE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
    }
}