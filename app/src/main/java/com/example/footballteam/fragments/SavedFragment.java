package com.example.footballteam.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import com.example.footballteam.MainActivity;
import com.example.footballteam.R;
import com.example.footballteam.data.Team;
import com.example.footballteam.util.DataUtil;
import com.example.footballteam.util.SavedListAdapter;

import org.json.JSONException;

import java.util.List;

import moxy.MvpAppCompatFragment;
import moxy.presenter.InjectPresenter;


public class SavedFragment extends MvpAppCompatFragment implements SavedPresenterViewInterface {
    private View view;

    @InjectPresenter
    SavedPresenter presenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_saved, container, false);

        presenter.setRecycler(getContext());

        ImageButton button = view.findViewById(R.id.saved_fragment_back_button);
        button.setOnClickListener(v -> getParentFragmentManager().popBackStack());

        Button gallery = view.findViewById(R.id.gallery_button);
        gallery.setOnClickListener(v -> {
            FragmentTransaction ft = getParentFragmentManager().beginTransaction();
            ft.replace(R.id.fragmentContainerView, new GalleryFragment()).addToBackStack(null).commit();
        });

        return view;
    }

    @Override
    public void showSaved(List<Team> teams) {
        RecyclerView recyclerView = view.findViewById(R.id.saved_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        SavedListAdapter adapter = new SavedListAdapter(teams);
        adapter.setListener(new SavedListAdapter.clickListener() {
            @Override
            public void qrButtonClick(int num) {
                try {
                    FragmentTransaction ft = getParentFragmentManager().beginTransaction();
                    ft.add(R.id.fragmentContainerView, new QrFragment(teams.get(num).toJson().toString())).addToBackStack(null).commit();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onElementClick(int num) {
                try{
                    Bundle b = new Bundle();
                    b.putString("team", teams.get(num).toJson().toString());
                    Intent i = new Intent(getContext(), MainActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    i.putExtras(b);
                    startActivity(i);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void deleteTeam(String name) {
                DataUtil.deleteTeam(getContext(), name);
            }
        });
        recyclerView.setAdapter(adapter);
    }
}