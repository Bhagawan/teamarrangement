package com.example.footballteam.fragments;

import com.example.footballteam.data.Team;

import java.util.List;

import moxy.MvpView;
import moxy.viewstate.strategy.alias.AddToEnd;

public interface SavedPresenterViewInterface extends MvpView {
    @AddToEnd
    void showSaved(List<Team> teams);


}
