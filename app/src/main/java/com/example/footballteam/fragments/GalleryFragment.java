package com.example.footballteam.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.example.footballteam.R;
import com.example.footballteam.data.GalleryImage;
import com.example.footballteam.util.DataUtil;
import com.example.footballteam.util.GalleryAdapter;

import java.util.List;


public class GalleryFragment extends Fragment {
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_gallery, container, false);
        showGallery();

        ImageButton button = view.findViewById(R.id.gallery_back_button);
        button.setOnClickListener(v -> getParentFragmentManager().popBackStack());

        return view;
    }

    private void showGallery() {
        List<GalleryImage> gallery = DataUtil.loadGallery(getContext());
        if(!gallery.isEmpty()) {
            RecyclerView recyclerView = view.findViewById(R.id.gallery_recycler);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
            GalleryAdapter adapter = new GalleryAdapter(gallery);
            recyclerView.setAdapter(adapter);
        }
    }
}