package com.example.footballteam;

import android.app.Application;

import androidx.appcompat.app.AppCompatDelegate;

import com.onesignal.OneSignal;

public class FootballApp extends Application {
    private static final String ONESIGNAL_APP_ID = "1c7d90c9-dc77-4342-9c79-8dab058e4521";

    @Override
    public void onCreate() {
        super.onCreate();

        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE);
        OneSignal.initWithContext(this);
        OneSignal.setAppId(ONESIGNAL_APP_ID);
        OneSignal.sendTag("Test","n");
    }
}
