package com.example.footballteam.data;

import org.json.JSONException;
import org.json.JSONObject;

public class Player {
    public String name = "";
    public double x = 0, y = 0;

    public Player(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Player(JSONObject object) throws JSONException {
        this.name = object.getString("name");
        this.x = object.getDouble("x");
        this.y = object.getDouble("y");
    }

    public void moveToDelta(float x, float y){
        this.x += x;
        this.y += y;
    }

    public void setName(String name) {this.name = name;}

}
