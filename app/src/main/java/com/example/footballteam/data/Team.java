package com.example.footballteam.data;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Team {
    private String name;
    private int color;
    private Player[] players = new Player[11];


    public Team(){}

    public Team(JSONObject object) throws JSONException {
        this.name = object.getString("name");
        this.color = object.getInt("color");
        JSONArray pl = object.getJSONArray("players");
        for(int i = 0; i < pl.length(); i++) {
            players[i] = new Player(pl.getJSONObject(i));
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public Player getPlayer(int player) {
        return players[player];
    }

    public void setPlayer(int n, Player player) {
        players[n] = player;
    }

    public JSONObject toJson() throws JSONException {
        JSONObject output = new JSONObject();
        output.put("name",name);
        output.put("color",color);

        JSONArray pl = new JSONArray();

        for (Player value : players) {
            JSONObject player = new JSONObject();
            player.put("name", value.name);
            player.put("x", value.x);
            player.put("y", value.y);
            pl.put(player);
        }
        output.put("players", pl);
        return output;
    }

}
