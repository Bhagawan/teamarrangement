package com.example.footballteam.data;

import android.graphics.Bitmap;

public class GalleryImage {
    private Bitmap bitmap;
    private String path;

    public GalleryImage(Bitmap bitmap, String path) {
        this.bitmap = bitmap;
        this.path = path;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public String getPath() {
        return path;
    }
}
